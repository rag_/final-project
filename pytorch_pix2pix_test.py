import torch, argparse, os
import network8 as network 
import matplotlib.pyplot as plt
from torchvision import transforms
from torch.autograd import Variable
import util
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', required=False, default='captcha',  help='')
parser.add_argument('--modelpath', required=False, default='captcha_16_8_0.0001_0.001_10.0_0.5_0.999_True_results',  help='')
parser.add_argument('--test_subfolder', required=False, default='test',  help='')

parser.add_argument('--input_size', type=int, default=256, help='input size')
parser.add_argument('--save_root', required=False, default='results', help='results save path')
parser.add_argument('--inverse_order', type=bool, default=True, help='0: [input, target], 1 - [target, input]')
opt = parser.parse_args()
print(opt)

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print("!!!",device,"!!!")

with open(os.path.join(opt.modelpath,'train_hist.pkl'), 'rb') as f:
    data_new = pickle.load(f)
    opt.ngf = data_new['opt'].ngf
print("create animation");    
util.generate_animation(opt.modelpath+"/results", data_new['opt'])

# data_loader
transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
])
test_loader = util.data_load('data/' + opt.dataset, opt.test_subfolder, transform, batch_size=1, shuffle=False)

test_result_path = opt.modelpath + '/test_results'
if not os.path.isdir(test_result_path):
    os.mkdir(test_result_path)

G = network.generator(opt.ngf)
G.to(device)
G.load_state_dict(torch.load(opt.modelpath + '/generator_param.pkl', map_location='cpu'))

img_size = int(256)
print(img_size)

# network
n = 0
print('test start!')
for x_, _ in test_loader:
    #print(x_.size())
    if opt.inverse_order:
        y_ = x_[:, :, :, img_size:]
        x_ = x_[:, :, :, 0:img_size]
    else:
        y_ = x_[:, :, :, 0:img_size]
        x_ = x_[:, :, :, img_size:]
    
    #print(x_.size())
    #print(y_.size())
    if x_.size()[3] != opt.input_size:
        x_ = util.imgs_resize(x_, opt.input_size)
        y_ = util.imgs_resize(y_, opt.input_size)

    y_ = Variable(y_.to(device), volatile=True)
    test_image = G(y_)
    s = test_loader.dataset.imgs[n][0][::-1]
    s_ind = len(s) - s.find('/')
    e_ind = len(s) - s.find('.')
    ind = test_loader.dataset.imgs[n][0][s_ind:e_ind-1]
    path = os.path.join(test_result_path , ind + '_target.png')
    plt.imsave(path, (x_[0].cpu().data.numpy().transpose(1, 2, 0) + 1) / 2)
    path = os.path.join(test_result_path , ind + '_output.png')
    plt.imsave(path, (test_image[0].cpu().data.numpy().transpose(1, 2, 0) + 1) / 2)
    path = os.path.join(test_result_path , ind + '_input.png')
    plt.imsave(path, (y_[0].numpy().transpose(1, 2, 0) + 1) / 2)

    n += 1

print('%d images generation complete!' % n)


