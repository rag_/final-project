#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 16:20:53 2019

@author: rag
"""


from PIL import Image, ImageDraw, ImageFont

import numpy as np
import os

def fontByHeigth (text, font_name, maxHeigth):
    fontsize = 1;
    font = ImageFont.truetype(font_name, fontsize)
    while font.getsize(text)[1] < maxHeigth:
        fontsize += 1
        font = ImageFont.truetype(font_name, fontsize)
    return  ImageFont.truetype(font_name, fontsize-1)

def fontByWidth (text, font_name, maxWidthByletter, amount):
    fontsize = 1;
    font = ImageFont.truetype(font_name, fontsize)
    while font.getsize(text)[0] < maxWidthByletter*amount:
        fontsize += 1
        font = ImageFont.truetype(font_name, fontsize)
    return ImageFont.truetype(font_name, fontsize-1)

def buildDecodeLabelImg(imgCaptcha, text, fontPath):
    width, height = imgCaptcha.size
    img = Image.new('RGB', (width, height), color = 'white')
    
    d = ImageDraw.Draw(img)
    
    letters = list(text)
    
    dx = width/(len(letters)*3+1)
    padding_y = width * 0.15 
    
    fontH = fontByHeigth(text, font_name, height*0.7)
    fontW = fontByWidth(text, font_name, dx*2, len(letters))
    #print(fontH.size)
    #print(fontW.size)
    font = fontW
    if fontH.size < font.size:
        font = fontH 
        
    for index in range(len(letters)):
        #print(letters[index]+":"+str(index))
        #print(((index*3+1)*dx,int(height*0.3)))
        d.text(((index*3+1)*dx,int(height*0.15)), letters[index], fill=(0,0,0), font = font)
    return img 

def contcaImages(img1, img2):
    width, height = img1.size
    new_im = Image.new('RGB', (width*2, height), (255, 255, 255))
    new_im.paste(img1, (0,0))
    new_im.paste(img2, (width,0))
    return new_im;


font_name = "/usr/share/fonts/gnu-free/FreeSans.ttf"

sourceDir = 'original-dataset'
resultDir = 'data/captcha'

if not os.path.isdir("data"):
    os.mkdir("data")
    
if not os.path.isdir(resultDir):
    os.mkdir(resultDir)    

subdirs = ["train", "test"] 

probs = [0.9, 0.1]

test_amount = 0;
for filename in os.listdir(sourceDir):
    if "?" in filename:
        continue
    
    subdir = np.random.choice(subdirs, p=probs)
    #print(os.path.join(subdir,filename))

    text = filename.split("-")[0]
        
    if subdir == 'test':
        test_amount = test_amount +1

    imgCaptcah = Image.open(os.path.join(sourceDir, filename))
    w ,h = imgCaptcah.size
    new_w = 0
    new_h = 0
    for index in range(1,10):
        new_size = np.power(2, index);
        if  new_w == 0 and new_size >= w:
            new_w = new_size
        if  new_h == 0 and new_size >= h:
            new_h = new_size
        if  new_h > 0  and new_w > 0:    
            break
    new_img = Image.new('RGB', (new_w, new_h), (255, 255, 255))
    new_img.paste(imgCaptcah, (0,0))
    imgCaptcah = new_img
    
    pureImg = buildDecodeLabelImg(imgCaptcah, text.upper(), font_name)
    doubleImg = contcaImages(imgCaptcah, pureImg)
    
    #pureImg.save("_"+fname)
    if not os.path.isdir(os.path.join(resultDir, subdir)):
        os.mkdir(os.path.join(resultDir, subdir))
    doubleImg.save(os.path.join(resultDir, subdir, filename))
    print("save ", os.path.join(resultDir, subdir, filename))

