naimport torch
import torch.nn as nn
import torch.nn.functional as F

class generator(nn.Module):
    # initializers
    def __init__(self, d=64):
        super(generator, self).__init__()
        # Unet encoder
        self.conv1 = nn.Conv2d(3, d, 4, 2, 1)
        self.conv2 = nn.Conv2d(d, d * 2, 4, 2, 1)
        self.conv2_bn = nn.BatchNorm2d(d * 2)
        self.conv3 = nn.Conv2d(d * 2, d * 4, 4, 2, 1)
        self.conv3_bn = nn.BatchNorm2d(d * 4)
        self.conv4 = nn.Conv2d(d * 4, d * 8, 4, 2, 1)
        self.conv4_bn = nn.BatchNorm2d(d * 8)
        self.conv5 = nn.Conv2d(d * 8, d * 8, 4, (1,2), 1)
        self.conv5_bn = nn.BatchNorm2d(d * 8)
        self.conv6 = nn.Conv2d(d * 8, d * 8, 4, (1,2), 1)
        self.conv6_bn = nn.BatchNorm2d(d * 8)
        self.conv7 = nn.Conv2d(d * 8, d * 8, 4, (1,2), 1)
      

        # Unet decoder
        self.deconv1 = nn.ConvTranspose2d(d * 8, d * 8, 4, (1,2), 1)
        self.deconv1_bn = nn.BatchNorm2d(d * 8)
        self.deconv2 = nn.ConvTranspose2d(d * 8 * 2, d * 8, 4, (1,2), 1)
        self.deconv2_bn = nn.BatchNorm2d(d * 8)
        self.deconv3 = nn.ConvTranspose2d(d * 8 * 2, d * 8, 4, (1,2), 1)
        self.deconv3_bn = nn.BatchNorm2d(d * 8)
        self.deconv4 = nn.ConvTranspose2d(d * 8 * 2, d * 4, 4, 2, 1)
        self.deconv4_bn = nn.BatchNorm2d(d * 4)
        self.deconv5 = nn.ConvTranspose2d(d * 4 * 2, d * 2, 4, 2, 1)
        self.deconv5_bn = nn.BatchNorm2d(d * 2)
        self.deconv6 = nn.ConvTranspose2d(d * 2 * 2, d , 4, 2, 1)
        self.deconv6_bn = nn.BatchNorm2d(d)
        self.deconv7 = nn.ConvTranspose2d(d * 2, 3, 4, 2, 1)
        
    # weight_init
    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m], mean, std)

    # forward method
    def forward(self, input):
        #print("0 ", input.size())
        e1 = self.conv1(input)
        #print("1 ", e1.size())
        e2 = self.conv2_bn(self.conv2(F.leaky_relu(e1, 0.2)))
        #print("2 ", e2.size())
        e3 = self.conv3_bn(self.conv3(F.leaky_relu(e2, 0.2)))
        #print("3 ", e3.size())
        e4 = self.conv4_bn(self.conv4(F.leaky_relu(e3, 0.2)))
        #print("4 ", e4.size())
        e5 = self.conv5_bn(self.conv5(F.leaky_relu(e4, 0.2)))
        #print("5 ", e5.size())
        e6 = self.conv6_bn(self.conv6(F.leaky_relu(e5, 0.2)))
        #print("6 ", e6.size())
        e7 = self.conv7(F.leaky_relu(e6, 0.2))
        #print("7 ", e7.size())
        
        d1 = F.dropout(self.deconv1_bn(self.deconv1(F.relu(e7))), 0.5, training=True)
        #print("1-6 ",d1.size(), e6.size())
        d1 = torch.cat([d1, e6], 1)
        d2 = F.dropout(self.deconv2_bn(self.deconv2(F.relu(d1))), 0.5, training=True)
        #print("2-5 ",d2.size(), e5.size())
        d2 = torch.cat([d2, e5], 1)
        d3 = F.dropout(self.deconv3_bn(self.deconv3(F.relu(d2))), 0.5, training=True)
        #print("3-4 ",d3.size(), e4.size())
        d3 = torch.cat([d3, e4], 1)
        d4 = self.deconv4_bn(self.deconv4(F.relu(d3)))        
        #print("4-3 ",d4.size(), e3.size())
        d4 = torch.cat([d4, e3], 1)
        d5 = self.deconv5_bn(self.deconv5(F.relu(d4)))
        #print("2-5 ",d5.size(), e2.size())
        d5 = torch.cat([d5, e2], 1)
        d6 = self.deconv6_bn(self.deconv6(F.relu(d5)))
        #print("6-1 ",d6.size(), e1.size())
        d6 = torch.cat([d6, e1], 1)
        d7 = self.deconv7(F.relu(d6))
        #print("7- ",d7.size())
        
        o = F.tanh(d7)

        return o

class discriminator(nn.Module):
    # initializers
    def __init__(self, d=64):
        super(discriminator, self).__init__()
        self.conv1 = nn.Conv2d(6, d, 4, 2, 1)
        #self.conv1_bn = nn.BatchNorm2d(d)
        self.conv2 = nn.Conv2d(d, d * 2, 4, 2, 1)
        self.conv2_bn = nn.BatchNorm2d(d * 2)
        self.conv3 = nn.Conv2d(d * 2, d * 4, 4, 2, 1)
        self.conv3_bn = nn.BatchNorm2d(d * 4)
        self.conv4 = nn.Conv2d(d * 4, 1, 4, 1, 1)
        

    # weight_init
    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m], mean, std)

    # forward method
    def forward(self, input, label):
        x = torch.cat([input, label], 1)
        #print("x ",x.size())
        #x = F.leaky_relu(self.conv1_bn(self.conv1(x)), 0.2)
        x = F.leaky_relu(self.conv1(x), 0.2)
        #print("d_1" ,x.size())
        x = F.leaky_relu(self.conv2_bn(self.conv2(x)), 0.2)
        #print("d_2" ,x.size())    
        x = F.leaky_relu(self.conv3_bn(self.conv3(x)), 0.2)
        #print("d_3" ,x.size())
        x = self.conv4(x)
        #print("d_3" ,x.size())
        x = F.sigmoid(x)
        #print(x.size())
        

        return x

def normal_init(m, mean, std):
    if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
        m.weight.data.normal_(mean, std)
        m.bias.data.zero_()
        
