## About task
I want to recognition captcha as Image. It is original dataset- image captha and text. It was transformed to image as concatinate images of capthca image and clear text image  [transformer.py](transformer.py). I use pix2pix model. Was modefied original code for rectangle images (generator/discriminator [network8.py](network8.py)). Was modefied code for executint model at cpu/gpu device and write save/load code at train proccess. 
So was writed utile code [parameters_iterator.py](parametrs_iterator.py) for random iteration the params of model
## Capthca recognition as Image
 * epoch : 62
 * genarator train params:
    * filters: x64, lerning rate: 0.0001, optimization parameter: 0.1
 * discriminator train params:
    * filters: x32, lerning rate: 0.0001, optimization parameter: 0.1
 * L1 lambda: 100   
 
Fisrt column - input, second - output, third - ground truth.
## Images
![recognition](result/train/61.png)
## Loss
![loss](result/losses/loss.png)

## Inverted task
I want to view how model can generate captcha by clear image.
## Generate capthca from clear text image;
 * epoch : 62  
 * genarator train params:
    * filters: x16, lerning rate: 0.0001, optimization parameter: 0.5
 * discriminator train params:
    * filters: x8, lerning rate: 0.001, optimization parameter: 0.999
 * L1 lambda: 10  
 
Fisrt column - input, second - output, third - ground truth.
## Images
![recognition](result_invert/train/62.png)
## Loss
![loss](result_invert/losses/62.png)

## Accuracy

| recognition  | amount   | percent |
| :----------- | :------  | :------ |
| 5 from 5     | 70       | 77.7    |
| 4 from 5     | 19       | 21.1    |
| 3 from 5     | 1        | 1.2     |
| 2 from 5     | 0        | 0       |  
| 1 from 5     | 0        | 0       |

## Reference
* [original code pip2pix](https://github.com/znxlwm/pytorch-pix2pix) - Pytorch implementation of pix2pix for various datasets.
* [Paper](https://www.lancaster.ac.uk/staff/wangz3/publications/ccs18.pdf) - Yet Another Text Captcha Solver
* [Paper](https://arxiv.org/pdf/1701.01081.pdf) - SalGAN: visual saliency prediction with adversarial networks
* [Paper](https://arxiv.org/pdf/1611.07004.pdf) - Image-to-Image Translation with Conditional Adversarial Networks

 