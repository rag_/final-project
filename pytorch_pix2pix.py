import os, time, pickle, argparse, util
import network8 as network
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import transforms
from torch.autograd import Variable

parser = argparse.ArgumentParser()

parser.add_argument('--dataset', required=False, default='captcha',  help='')

parser.add_argument('--train_subfolder', required=False, default='train',  help='')
parser.add_argument('--test_subfolder', required=False, default='test',  help='')
parser.add_argument('--batch_size', type=int, default=10, help='train batch size')
parser.add_argument('--test_batch_size', type=int, default=5, help='test batch size')


parser.add_argument('--ngf', type=int, default=8)
parser.add_argument('--ndf', type=int, default=8)
parser.add_argument('--input_size', type=int, default=256, help='input size')

parser.add_argument('--train_epoch', type=int, default=50, help='number of train epochs')
parser.add_argument('--lrD', type=float, default=0.0011, help='learning rate, default=0.0002')
parser.add_argument('--lrG', type=float, default=0.0011, help='learning rate, default=0.0002')
parser.add_argument('--L1_lambda', type=float, default=100, help='lambda for L1 loss')
parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for Adam optimizer')
parser.add_argument('--beta2', type=float, default=0.999, help='beta2 for Adam optimizer')
parser.add_argument('--save_root', required=False, default='results', help='results save path')
parser.add_argument('--inverse_order', type=bool, default=False, help='0: [input, target], 1 - [target, input]')
opt = parser.parse_args()
print(opt)

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print("!!!",device,"!!!")

root_list = []
root_list.append(opt.dataset)
root_list.append(str(opt.ngf))
root_list.append(str(opt.ndf))
root_list.append(str(opt.lrG))
root_list.append(str(opt.lrD))
root_list.append(str(opt.L1_lambda))
root_list.append(str(opt.beta1))
root_list.append(str(opt.beta2))
root_list.append(str(opt.inverse_order))
root_list.append(opt.save_root)

root ='_'.join(root_list)

print("view result in: ", root)

# results save path
dirs = [];
dirs.append(root)

result_path =  root + '/results'
dirs.append(result_path)

loss_path =  root + '/losses'
dirs.append(loss_path)

generator_params_path =  root + '/generator_params'
dirs.append(generator_params_path)

discriminator_params_path =  root + '/discriminator_params'
dirs.append(discriminator_params_path)

for new_path in dirs:
    if not os.path.isdir(new_path):
        os.mkdir(new_path)

# data_loader
transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
])
train_loader = util.data_load('data/' + opt.dataset, opt.train_subfolder, transform, opt.batch_size, shuffle=True)
test_loader = util.data_load('data/' + opt.dataset, opt.test_subfolder, transform, opt.test_batch_size, shuffle=False)
test = test_loader.__iter__().__next__()[0]
img_size = int(test.size()[3]/2)
print(img_size)
if opt.inverse_order:
    fixed_y_ = test[:, :, :, 0:img_size]
    fixed_x_ = test[:, :, :, img_size:]
else:
    fixed_x_ = test[:, :, :, 0:img_size]
    fixed_y_ = test[:, :, :, img_size:]

if img_size != opt.input_size:
    fixed_x_ = util.imgs_resize(fixed_x_, opt.input_size)
    fixed_y_ = util.imgs_resize(fixed_y_, opt.input_size)

    
train_hist = {}
train_hist['D_losses'] = []
train_hist['G_losses'] = []
train_hist['per_epoch_ptimes'] = []
train_hist['total_ptime'] = []
train_hist['opt'] = opt

# network
G = network.generator(opt.ngf)
D = network.discriminator(opt.ndf)
G.weight_init(mean=0.0, std=0.02)
D.weight_init(mean=0.0, std=0.02)

# Adam optimizer
G_optimizer = optim.Adam(G.parameters(), lr=opt.lrG, betas=(opt.beta1, opt.beta2))
D_optimizer = optim.Adam(D.parameters(), lr=opt.lrD, betas=(opt.beta1, opt.beta2))

# loss
BCE_loss = nn.BCELoss().to(device)
L1_loss = nn.L1Loss().to(device)


start_epoch=0;

# find save model.
max_generator_num = -1
max_discriminator_num = -1
for filename in os.listdir(generator_params_path):
    num = int(filename.split('.')[0])
    if (num > max_generator_num):
        max_generator_num = num
for filename in os.listdir(discriminator_params_path):
    num = int(filename.split('.')[0])
    if (num > max_discriminator_num):
        max_discriminator_num = num
max_saved_epoch = -1       
if (max_generator_num > -1 and max_discriminator_num > -1):
    if max_generator_num > max_discriminator_num:
        max_saved_epoch = max_generator_num
    else:
        max_saved_epoch = max_discriminator_num

# restore save_model
if max_saved_epoch > -1:
    G_checkpoint = torch.load(os.path.join(generator_params_path, str(max_saved_epoch)+".pkl"))
    G.load_state_dict(G_checkpoint['G_model_state_dict'])
    G_optimizer.load_state_dict(G_checkpoint['G_optimizer_state_dict'])
    for state in G_optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.to(device)
    start_epoch = G_checkpoint['epoch']
    train_hist['G_losses']  = G_checkpoint['G_loss']
    
    D_checkpoint = torch.load(os.path.join(discriminator_params_path, str(max_saved_epoch)+".pkl"))
    D.load_state_dict(D_checkpoint['D_model_state_dict'])
    D_optimizer.load_state_dict(D_checkpoint['D_optimizer_state_dict'])
    for state in D_optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.to(device)

    start_epoch = D_checkpoint['epoch']
    train_hist['D_losses']  = D_checkpoint['D_loss']
    


G.to(device)
D.to(device)

G.train()
D.train()

print('training start!')
start_time = time.time()
for epoch in range(start_epoch, opt.train_epoch):
    D_losses = []
    G_losses = []
    epoch_start_time = time.time()
    num_iter = 0
    for x_, _ in train_loader:
        
        D.zero_grad()

        if opt.inverse_order:
            y_ = x_[:, :, :, 0:img_size]
            x_ = x_[:, :, :, img_size:]
        else:
            y_ = x_[:, :, :, img_size:]
            x_ = x_[:, :, :, 0:img_size]
            
        #print(x_.size(), y_.size())        

        x_, y_ = Variable(x_.to(device)), Variable(y_.to(device))
        
        
        # train discriminator D

        D_result = D(x_, y_).squeeze()
        D_real_loss = BCE_loss(D_result, Variable(torch.ones(D_result.size()).to(device)))

        G_result = G(x_)
        D_result = D(x_, G_result).squeeze()
        D_fake_loss = BCE_loss(D_result, Variable(torch.zeros(D_result.size()).to(device)))

        D_train_loss = (D_real_loss + D_fake_loss) * 0.5
        D_train_loss.backward()
        D_optimizer.step()

        #log train D
        train_hist['D_losses'].append(D_train_loss.data.item())
        D_losses.append(D_train_loss.data.item())

        # train generator G
        G.zero_grad()

        G_result = G(x_)
        D_result = D(x_, G_result).squeeze()
        
        #print("D", D_result.size())
        #print("M I ", torch.ones(D_result.size()).size())
        #print("G ", G_result.size())

        G_train_loss = BCE_loss(D_result, Variable(torch.ones(D_result.size()).to(device))) + opt.L1_lambda * L1_loss(G_result, y_)
        G_train_loss.backward()
        G_optimizer.step()

        #log train D
        train_hist['G_losses'].append(G_train_loss.data.item())
        G_losses.append(G_train_loss.data.item())
            
        num_iter += 1
        if num_iter % 100 == 0:
            print('%d  ptime: %.2f G_LOSS: %.3f D_LOSS: %.3f' % (num_iter, time.time(), G_losses[-1], D_losses[-1]))

    epoch_end_time = time.time()
    per_epoch_ptime = epoch_end_time - epoch_start_time

    print('[%d/%d] - ptime: %.2f, loss_d: %.3f, loss_g: %.3f' % ((epoch + 1), opt.train_epoch, per_epoch_ptime, torch.mean(torch.FloatTensor(D_losses)),
                                                             torch.mean(torch.FloatTensor(G_losses))))
    if epoch % 4 == 0:    
        fixed_result = os.path.join(result_path, str(epoch + 1) + '.png')
        util.show_result(G, Variable(fixed_x_.to(device), volatile=True), fixed_y_, (epoch+1), save=True, path=fixed_result)
        
        fixed_loss = os.path.join(loss_path, 'loss.png')
        util.show_train_hist(train_hist, False, True,   fixed_loss);
        
    train_hist['per_epoch_ptimes'].append(per_epoch_ptime)
    
    if epoch % 20 == 0 and  epoch > 0 :
        torch.save({
            'epoch': epoch,
            'G_model_state_dict': G.state_dict(),
            'G_optimizer_state_dict': G_optimizer.state_dict(),
            'G_loss': train_hist['G_losses'],
            }, os.path.join(generator_params_path, str(epoch)+'.pkl'))
        torch.save({
            'epoch': epoch,
            'D_model_state_dict': D.state_dict(),
            'D_optimizer_state_dict': D_optimizer.state_dict(),
            'D_loss': train_hist['D_losses'],
            }, os.path.join(discriminator_params_path, str(epoch)+'.pkl'))

end_time = time.time()
total_ptime = end_time - start_time
train_hist['total_ptime'].append(total_ptime)

print("Avg one epoch ptime: %.2f, total %d epochs ptime: %.2f" % (torch.mean(torch.FloatTensor(train_hist['per_epoch_ptimes'])), opt.train_epoch, total_ptime))
print("Training finish!... save training results")
torch.save(G.state_dict(), os.path.join(root,'generator_param.pkl'))
torch.save(D.state_dict(), os.path.join(root,'discriminator_param.pkl'))

with open(os.path.join(root,'train_hist.pkl'), 'wb') as f:
    pickle.dump(train_hist, f)

util.generate_animation(loss_path, opt)


#with open(os.path.join(root,'train_hist.pkl'), 'rb') as f:
#    data_new = pickle.load(f)
