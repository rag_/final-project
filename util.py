import itertools, imageio, torch
import matplotlib.pyplot as plt
import numpy as np
import os
from torchvision import datasets
from scipy.misc import imresize

def show_result(G, x_, y_, num_epoch, show = False, save = False, path = 'result.png'):
    # G.eval()
    test_images = G(x_)

    size_figure_grid = 3
    fig, ax = plt.subplots(x_.size()[0], size_figure_grid, figsize=(30, 30))
    for i, j in itertools.product(range(x_.size()[0]), range(size_figure_grid)):
        ax[i, j].get_xaxis().set_visible(False)
        ax[i, j].get_yaxis().set_visible(False)

    for i in range(x_.size()[0]):
        ax[i, 0].cla()
        ax[i, 0].imshow((x_[i].cpu().data.numpy().transpose(1, 2, 0) + 1) / 2)
        ax[i, 1].cla()
        ax[i, 1].imshow((test_images[i].cpu().data.numpy().transpose(1, 2, 0) + 1) / 2)
        ax[i, 2].cla()
        ax[i, 2].imshow((y_[i].numpy().transpose(1, 2, 0) + 1) / 2)

    label = 'Epoch {0}'.format(num_epoch)
    fig.text(0.5, 0.04, label, ha='center')

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

def show_train_hist(hist, show = False, save = False, path = 'Train_hist.png'):
    x = range(len(hist['D_losses']))

    y1 = hist['D_losses']
    y2 = hist['G_losses']
    
    last_iter_len = 85
    if len(x) < last_iter_len:
        last_iter_len = len(x) - 1
    x_last = x[-last_iter_len:]
    y1_last = y1[-last_iter_len: ]
    y2_last = y2[-last_iter_len:] 
    
    fig, axes = plt.subplots(nrows = 3, ncols = 2, squeeze=False, figsize=(10, 10))
    
    
    
    axes[0,0].plot(x, y1, "green", label="D_loss")
    axes[0,0].plot(x, y2, "blue", label="G_loss")
    axes[0,0].set_title("D_G_Loss")
    axes[0,0].legend(loc ="best")
    axes[0,0].set_xlabel("iteration")
    axes[0,0].set_ylabel("loss")
    axes[0,0].grid()
    
    axes[1,0].plot(x, y1, "green")
    axes[1,0].set_title("D_Loss")
    axes[1,0].set_xlabel("iteration")
    axes[1,0].set_ylabel("loss")
    axes[1,0].grid()
    
    axes[2,0].plot(x, y2, "blue")
    axes[2,0].set_title("G_Loss")
    axes[2,0].set_xlabel("iteration")
    axes[2,0].set_ylabel("loss")
    axes[2,0].grid()
    
    
    axes[0,1].plot(x_last, y1_last, "green", label="D_loss")
    axes[0,1].plot(x_last, y2_last, "blue", label="G_loss")
    axes[0,1].set_title("D_G_Loss_window")
    axes[0,1].legend(loc ="best")
    axes[0,1].set_xlabel("iteration")
    axes[0,1].set_ylabel("loss")
    axes[0,1].grid()
    
    axes[1,1].plot(x_last, y1_last, "green")
    axes[1,1].set_title("D_Loss_window")
    axes[1,1].set_xlabel("iteration")
    axes[1,1].set_ylabel("loss")
    axes[1,1].grid()
    
    axes[2,1].plot(x_last, y2_last, "blue")
    axes[2,1].set_title("G_Loss_window")
    axes[2,1].set_xlabel("iteration")
    axes[2,1].set_ylabel("loss")
    axes[2,1].grid()
    
    fig.tight_layout()
    if save:
        fig.savefig(path)
        
    plt.close(fig)

'''
    plt.plot(x, y1, label='D_loss')
    plt.plot(x, y2, label='G_loss')

    plt.xlabel('Iter')
    plt.ylabel('Loss')

    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()
'''

def generate_animation(result_path, opt):
    images = []
    for e in range(opt.train_epoch):        
        img_name = os.path.join(result_path , str(e + 1) + '.png')
        if os.path.isfile(img_name):        
            images.append(imageio.imread(img_name))
    imageio.mimsave(os.path.join(result_path, 'generate_animation.gif'), images, fps=5)

def data_load(path, subfolder, transform, batch_size, shuffle=True):
    dset = datasets.ImageFolder(path, transform)
    ind = dset.class_to_idx[subfolder]
    
    n = 0
    for i in range(dset.__len__()):
        if ind != dset.imgs[n][1]:
            del dset.imgs[n]
            n -= 1

        n += 1

    return torch.utils.data.DataLoader(dset, batch_size=batch_size, shuffle=shuffle)

def imgs_resize(imgs, resize_scale_h = 143, resize_scale_w=286):
    outputs = torch.FloatTensor(imgs.size()[0], imgs.size()[1], resize_scale_h, resize_scale_w)
    for i in range(imgs.size()[0]):
        img = imresize(imgs[i].numpy(), [resize_scale_h, resize_scale_w])
        outputs[i] = torch.FloatTensor((img.transpose(2, 0, 1).astype(np.float32).reshape(-1, imgs.size()[1], resize_scale_h, resize_scale_w) - 127.5) / 127.5)

    return outputs

def random_crop(imgs1, imgs2, crop_size_h = 128, crop_size_w = 256):
    outputs1 = torch.FloatTensor(imgs1.size()[0], imgs1.size()[1], crop_size_h, crop_size_w)
    outputs2 = torch.FloatTensor(imgs2.size()[0], imgs2.size()[1], crop_size_h, crop_size_w)
    for i in range(imgs1.size()[0]):
        img1 = imgs1[i]
        img2 = imgs2[i]
        
        rand1 = np.random.randint(0, imgs1.size()[2] - crop_size_h)
        rand2 = np.random.randint(0, imgs2.size()[2] - crop_size_h)
        outputs1[i] = img1[:, rand1: crop_size_h + rand1, rand2: crop_size_w + rand2]
        outputs2[i] = img2[:, rand1: crop_size_h + rand1, rand2: crop_size_w + rand2]

    return outputs1, outputs2

def random_fliplr(imgs1, imgs2):
    outputs1 = torch.FloatTensor(imgs1.size())
    outputs2 = torch.FloatTensor(imgs2.size())
    for i in range(imgs1.size()[0]):
        if torch.rand(1)[0] < 0.5:
            img1 = torch.FloatTensor(
                (np.fliplr(imgs1[i].numpy().transpose(1, 2, 0)).transpose(2, 0, 1).reshape(-1, imgs1.size()[1], imgs1.size()[2], imgs1.size()[3]) + 1) / 2)
            outputs1[i] = (img1 - 0.5) / 0.5
            img2 = torch.FloatTensor(
                (np.fliplr(imgs2[i].numpy().transpose(1, 2, 0)).transpose(2, 0, 1).reshape(-1, imgs2.size()[1], imgs2.size()[2], imgs2.size()[3]) + 1) / 2)
            outputs2[i] = (img2 - 0.5) / 0.5
        else:
            outputs1[i] = imgs1[i]
            outputs2[i] = imgs2[i]

    return outputs1, outputs2
