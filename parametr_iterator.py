#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 18:26:59 2019

@author: rag
"""

import subprocess
import os
import numpy as np

nf = [8,16]

lrD = [0.0001]
lrG = [0.0001]

L1_lambda =[100, 50]

beta1 = [0.5, 0.999] 
beta2 = [0.5, 0.999]

inverse_order = [0,1]

path = os.path.dirname(os.path.abspath(__file__))
print("path=", path)



amount_iteration = 1
lambda_index = 1
for index in range(amount_iteration):
    args = ['python3 pytorch_pix2pix.py']
    nf_val = np.random.choice(nf,1)[0]
    args.append('ngf '+ str(2*nf_val))
    args.append('ndf '+ str(nf_val))
    args.append('lrG '+ str(np.random.choice(lrG,1)[0]))
    args.append('lrD '+ str(np.random.choice(lrD,1)[0]))
    args.append('L1_lambda '+ str(np.random.choice(L1_lambda,1)[0]))
    args.append('beta1 '+ str(np.random.choice(beta1,1)[0]))
    args.append('beta2 '+ str(np.random.choice(beta2,1)[0]))
#    args.append('inverse_order '+str(inverse_order[0]))
    cmd = ' --'.join(args)
    print(cmd)
    pipe = subprocess.Popen(cmd, cwd = path, shell=True)
    output = pipe.communicate()
    print (output[0])
    print(cmd)
